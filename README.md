

## Download the following:
* https://code.google.com/p/google-api-php-client/
* Add to sites/all/libraries folder with the folder name 'google-api-php-client'

## Register an application through the google api console
* https://code.google.com/apis/console/
* Enable Google API service in the google api console.

## TODO

* 'admin/config/services/content-author' should return a table list instead of a form.

## Focus

* Google+
* Twitter

## Nice to have

* Facebook
* Pinterest
