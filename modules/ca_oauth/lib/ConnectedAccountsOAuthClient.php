<?php

/**
 * ConnectedAccountsOAuthClient.
 */
class ConnectedAccountsOAuthClient extends DrupalOAuthClient {
  function __construct($consumer, $request_token = NULL, $signature_method = NULL, $version = NULL) {
    parent::__construct($consumer, $request_token, $signature_method, $version);
  }
}
