<?php 

/**
 * Implements hook_content_author_settings_form().
 */
function ca_google_plus_content_author_settings_form($form, &$form_state) {
  $application_name = variable_get('site_name', '');
  $client_credentials = $form_state['ca_google_plus'];
  $client_id = '';
  $client_secret = '';
  $redirect_url = '';

  if ($client_credentials['csid']) {
    $consumer = DrupalOAuthConsumer::loadById($client_credentials['csid']);
    $client_id = $consumer->key;
    $client_secret = $consumer->secret;
    $redirect_url = $consumer->configuration['redirect_uri'];
    $form_state['consumer'] = $consumer;
  }

  $form['ca_google_plus'] = array(
    '#tree' => TRUE,
  );

  $form['ca_google_plus']['ca_id'] = array(
    '#type' => 'hidden',
    '#value' => $client_credentials['ca_id'],
  );

  $form['ca_google_plus']['ca_google_plus_application_name_description'] = array(
    '#type' => 'item',
    '#title' => t('Application name'),
    '#attributes' => array(
      'disabled' => TRUE,
    ),
    '#markup' => $application_name,
    '#description' => t('The name of your application. We recommend that to you ' .
      'keep it to @application_name. To change that, go to !site_information_link.', 
      array('@application_name' => $application_name, 
        '!site_information_link' => l('site information settings', 'admin/config/system/site-information'))),
  );

  $form['ca_google_plus']['application_name'] = array(
    '#type' => 'hidden',
    '#value' => $application_name,
  );

  $form['ca_google_plus']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => $client_id,
  );

  $form['ca_google_plus']['secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => $client_secret,
  );

  $form['ca_google_plus']['redirect_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URI'),
    '#default_value' => $redirect_url,
  );

  // Extended settings.
  $extended_settings['provider_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Base url',
    '#default_value' => 'https://accounts.google.com/',
    '#description' => t('The base url of authorization. Keep the trailing slash')
  );
  
  // Authorization endpoint.
  $extended_settings['authorization_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorization Endpoint'),
    '#default_value' => 'o/oauth2/auth',
    '#description' => t('The endpoint to where you application is authenticate to.'),
  );
  
  // Access endpoint.
  $extended_settings['access_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Token Endpoint'),
    '#default_value' => 'o/oauth2/token',
    '#description' => t('The endpoint on to which we would want to access our token'),
  );
  
  // The scope of access ( google+ api specific )
  $extended_settings['scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Scope'),
    '#default_value' => 'https://www.googleapis.com/auth/plus.login',
    '#description' => t('The scope of the authentication. Since Connected '.
      'Accounts is related to the account in your application, we will leave this '.
      'as it is.<br/> Read more at !authorize', array('!authorize' => 
      l('Goole Plus: Authorizing requests', 'https://developers.google.com/+/api/oauth#authorizing',
      array('attributes' => array('target' => '_blank'))))),
    '#attributes' => array(
    'disabled' => TRUE,
    ),
  );

  $extended_settings['people'] = array(
    '#type' => 'textfield',
    '#title' => t('People Base URL'),
    '#default_value' => 'https://www.googleapis.com/plus/v1/people/',
    '#description' => t('base uri for fetching profile fields and activities'),
  );

  $form['ca_google_plus']['extended_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Extended settings'),
    '#description' => t('Make sure that you have read the google api ' .
      'before you proceed to edit the extended settings of this client.'),
  );

  foreach ($extended_settings as $extended => $settings) {
    $form['ca_google_plus']['extended_settings'][$extended] = $settings;
  }

  // Consider: Adding "allowed fields" into the settings for this client.
  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#suffix' => l(t('Cancel'), 'admin/config/services/content-author'),
    ),
  );

  // @TODO: Rename these.
  // note: Right now I'm too lazy to do this.
  $form['#validate'] = array('ca_google_plus_content_author_client_form_validate');
  $form['#submit'] = array('ca_google_plus_content_author_client_form_submit');
  return $form;
}

/**
 * Validates ca_google_plus_content_author_settings_form()
 */
function ca_google_plus_content_author_client_form_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $google_plus = $values['ca_google_plus'];
  $config = array();

  foreach ($google_plus['extended_settings'] as $key => $value) {
    $config[$key] = $value;
  }
  unset($google_plus['extended_settings']);
  unset($google_plus['key']);
  unset($google_plus['secret']);
  foreach ($google_plus as $key => $value) {
    $config[$key] = $value;
  }
  $form_state['storage']['client'] = $config;
}

/**
 * Submits ca_google_plus_content_author_settings_form().
 */
function ca_google_plus_content_author_client_form_submit(&$form, &$form_state) {

  $values = $form_state['values'];
  $ca_google_plus = $values['ca_google_plus'];
  $client_id = $ca_google_plus['key'];
  $client_secret = $ca_google_plus['secret'];
  $new = FALSE;

  $config = $form_state['storage']['client'];

  if (!empty($form_state['consumer'])) {
    $consumer = $form_state['consumer'];
    $consumer->key = $client_id;
    $consumer->secret = $client_secret;
    $consumer->configuration = $config;
  }
  else {
    $consumer = new DrupalOAuthConsumer($client_id, $client_secret, $config);  
    // Indicate that this is a new consumer we are saving.
    $new = !$consumer->in_database;
  }
  
  // Save the consumer into {oauth_common_consumer} schema.
  $consumer->write();

  // Is the consumer saved?
  // Proceed to saving our client within the internal {connected_accounts}
  if ($consumer->in_database) {
    if ($new) {
      $object = new stdClass;
      $object->ca_id = $ca_google_plus['ca_id'];
      $object->csid = $consumer->csid;
      $saved = drupal_write_record('connected_accounts_client_enabled', $object, array('ca_id'));
    }
    drupal_set_message(t('Settings saved.'));
  }
  else {
    drupal_set_message(t('Saving your consumer has failed.'));
  }
}
