<?php

function content_author_settings_client_add_form($form, &$form_state) {
  $form = array();
  $settings_info = module_invoke_all('content_author_settings_info');
  if (!count($settings_info)) {
    $form['message'] = array(
      '#type' => 'item',
      '#markup' => 'please enable some submodules',
    );
    return $form;
  }
  foreach ($settings_info as $client => $values) {
    $option[$client] = $values['name'];
  }
  $form['add'] = array(
    '#type' => 'select',
    '#options' => $option,
    '#title' => 'something',
  );

  $form['actions'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Add'),
    ),
  );
  return $form;
}

function content_author_settings_client_add_form_submit(&$form, &$form_state) {
  // $settings_info = module_invoke_all('content_author_settings_info');
  $values = $form_state['values'];
  $build = array(
    'ca_name' => $values['add'],
  );
  drupal_write_record('connected_accounts_client_enabled', $build);
  drupal_goto('admin/config/services/content-author');
}

/**
 * Settings page.
 */
function content_author_settings_page() {
  $settings_info = module_invoke_all('content_author_settings_info');
  // No sub-modules on?
  // Return a message.
  if (!count($settings_info)) {
    return t('Please enable one or two sub-modules to content author. Go to '.
      '!modules', array('!modules' => l('modules', 'admin/modules')));
  }

  $header = array(
    'Client', 'Description', 'Settings',
  );

  // Thoughts: we should have a more specific check on which keys are
  // available to us before we add each client as a row to our settings
  // table.
  foreach ($settings_info as $client => $column) {
    $rows = array();
    foreach ($column as $key => $value) {
      if ($key == 'reference') {
        $value = l('settings', $value );
      }
      $row[$key] = $value;
    }
    $rows[] = $row;
  }

  // @TODO: Add a little bit of description into this page.
  return theme('table', array('header' => $header, 'rows' => $rows));
}

// @TODO: make use of a consumer id instead of a $client string.
function content_author_settings_adnan_page($client) {
  $form = array();
  $client_name = $client['ca_name'];
  $client_id = $client['ca_id'];
  $form_id = $client_name . '_content_author_settings_form';
  $form_state = form_state_defaults();
  $form_state[$client_name] = $client;
  $form = drupal_build_form($form_id, $form_state);
  return $form;
}

